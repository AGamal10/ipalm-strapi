const evnt6create = async (model) => {
    EVNT.evnt5create(model);
}

const evnt7create = async (model) => {
    let curDate = new Date().getTime();
    strapi.controllers.action.create({
        request: {
            body: {
                Type: strapi.config.currentEnvironment.EVNT_07_ACTIONTYPE,
                Description: strapi.config.currentEnvironment.EVNT_07_DESCRIPTION,
                RelatedAsset: model.qr_value,
                Status: strapi.config.currentEnvironment.EVNT_PENDING,
                DueDate: new Date(curDate + 28 * 24 * 60 * 60 * 1000).toISOString(),
                supervisor_id: model.supervisor_id,
                RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_TRAP
            }
        }
    })
}

const EVNT = {
    evnt0create: async (model) => {
        strapi.controllers.action.create({
            request: {
                body: {
                    Type: strapi.config.currentEnvironment.EVNT_00_ACTIONTYPE,
                    Description: strapi.config.currentEnvironment.EVNT_00_DESCRIPTION,
                    RelatedAsset: model.qr_value,
                    Status: strapi.config.currentEnvironment.EVNT_PENDING,
                    DueDate: new Date().toISOString(),
                    supervisor_id: model.supervisor_id,
                    RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_PALM
                }
            }
        });
    },

    evnt0close_evnt1create: async (model) => {
        let evnt0 = await strapi.controllers.action.find({
            query: {
                Type: strapi.config.currentEnvironment.EVNT_00_ACTIONTYPE,
                RelatedAsset: model.qr_value,
                supervisor_id: model.supervisor_id,
                RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_PALM
            }
        });

        if (evnt0.length === 0) {
            console.log('no EVNT-0 exist for this palm');
        }
        else {

            strapi.controllers.action.update({
                params: {
                    _id: evnt0[0]._id
                },
                request: {
                    body: {
                        Status: strapi.config.currentEnvironment.EVNT_DONE
                    }
                }
            });
        }

        let curDate = new Date();

        strapi.controllers.action.create({
            request: {
                body: {
                    Type: strapi.config.currentEnvironment.EVNT_01_ACTIONTYPE,
                    Description: strapi.config.currentEnvironment.EVNT_01_DESCRIPTION,
                    RelatedAsset: model.qr_value,
                    Status: strapi.config.currentEnvironment.EVNT_PENDING,
                    DueDate: new Date(curDate.getTime() + 48 * 60 * 60 * 1000).toISOString(),
                    supervisor_id: model.supervisor_id,
                    RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_PALM
                }
            }
        });
    },

    evnt1Close: async (model) => {
        let palm = await strapi.controllers.palm.find({
            query: { qr_value: model.qr_value }
        });
        palm = JSON.parse(palm.body);
        if (palm.items.length === 0) {
            console.log('length = 0');
        }
        else {
            palm = palm.items[0];

            let evnt01 = await strapi.controllers.action.find({
                query: {
                    Type: strapi.config.currentEnvironment.EVNT_01_ACTIONTYPE,
                    RelatedAsset: palm.qr_value,
                    Status: strapi.config.currentEnvironment.EVNT_PENDING,
                    supervisor_id: palm.supervisor_id,
                    RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_PALM
                }
            });
            console.log(evnt01);
            if (evnt01.length === 0) {
                console.log('evnt01 length = 0');
            }
            else {
                for (let evnt of evnt01) {
                    strapi.controllers.action.update({
                        params: { _id: evnt._id },
                        request: {
                            body: {
                                Status: strapi.config.currentEnvironment.EVNT_DONE
                            }
                        }
                    });
                }
            }
        }
    },

    evnt2Trigger: async (model) => {
        return model.health_status === strapi.config.currentEnvironment.HEALTHSTATUS_INFECTED;
    },

    evnt2Create: async (model) => {
        strapi.controllers.action.create({
            request: {
                body: {
                    Type: strapi.config.currentEnvironment.EVNT_02_ACTIONTYPE,
                    Description: strapi.config.currentEnvironment.EVNT_02_DESCRIPTION,
                    RelatedAsset: model.qr_value,
                    Status: strapi.config.currentEnvironment.EVNT_PENDING,
                    DueDate: new Date().toISOString(),
                    supervisor_id: model.supervisor_id,
                    RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_PALM
                }
            }
        });
    },

    evnt2Close: async (model) => {

        let evnt02 = await strapi.controllers.action.find({
            query: {
                Type: strapi.config.currentEnvironment.EVNT_02_ACTIONTYPE,
                RelatedAsset: model.qr_value,
                Status: strapi.config.currentEnvironment.EVNT_PENDING,
                supervisor_id: model.supervisor_id,
                RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_PALM
            }
        });
        console.log(model.qr_value, model.supervisor_id);
        if (evnt02.length === 0) {
            console.log('evnt02 length = 0');
        }
        else {
            for (let evnt of evnt02) {
                strapi.controllers.action.update({
                    params: { _id: evnt._id },
                    request: {
                        body: {
                            Status: strapi.config.currentEnvironment.EVNT_DONE
                        }
                    }
                });
            }
        }
    },

    evnt3Create: async (model) => {
        let curDate = new Date().getTime();
        strapi.controllers.action.create({
            request: {
                body: {
                    Type: strapi.config.currentEnvironment.EVNT_03_ACTIONTYPE,
                    Description: strapi.config.currentEnvironment.EVNT_03_DESCRIPTION,
                    RelatedAsset: model.qr_value,
                    Status: strapi.config.currentEnvironment.EVNT_PENDING,
                    DueDate: new Date(curDate + 7 * 24 * 60 * 60 * 1000).toISOString(),
                    supervisor_id: model.supervisor_id,
                    RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_PALM
                }
            }
        });
    },

    evnt4create: async (palm) => {
        let curDate = new Date().getTime();
        strapi.controllers.action.create({
            request: {
                body: {
                    Type: strapi.config.currentEnvironment.EVNT_04_ACTIONTYPE,
                    Description: strapi.config.currentEnvironment.EVNT_04_DESCRIPTION,
                    RelatedAsset: palm.qr_value,
                    Status: strapi.config.currentEnvironment.EVNT_PENDING,
                    DueDate: new Date(curDate + strapi.config.currentEnvironment.EVNT_CHECKDURATIONAFTERCLEAN[Math.min(3, palm.consecutive_clean_reports)] * 24 * 60 * 60 * 1000).toISOString(),
                    supervisor_id: palm.supervisor_id,
                    RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_PALM
                }
            }
        });
    },

    evnt5create: async (model) => {
        let curDate = new Date().getTime();
        strapi.controllers.action.create({
            request: {
                body: {
                    Type: strapi.config.currentEnvironment.EVNT_05_ACTIONTYPE,
                    Description: strapi.config.currentEnvironment.EVNT_05_DESCRIPTION,
                    RelatedAsset: model.qr_value,
                    Status: strapi.config.currentEnvironment.EVNT_PENDING,
                    DueDate: new Date(curDate + 14 * 24 * 60 * 60 * 1000).toISOString(),
                    supervisor_id: model.supervisor_id,
                    RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_TRAP
                }
            }
        })
    },

    evnt5close: async (model) => {
        let trap = await strapi.controllers.trap.find({
            query: {
                qr_value: model.qr_value
            }
        });
        if (trap.length === 0) {
            console.log('no traps with this qr');
        }
        else {
            trap = trap[0];
            let evnt05 = await strapi.controllers.action.find({
                query: {
                    Type: strapi.config.currentEnvironment.EVNT_05_ACTIONTYPE,
                    RelatedAsset: model.qr_value,
                    Status: strapi.config.currentEnvironment.EVNT_PENDING,
                    supervisor_id: trap.supervisor_id,
                    RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_TRAP
                }
            });
            if (evnt05.length === 0) {
                console.log('evnt05 length = 0');
            }
            else {
                for (let evnt of evnt05) {
                    strapi.controllers.action.update({
                        params: { _id: evnt._id },
                        request: {
                            body: {
                                Status: strapi.config.currentEnvironment.EVNT_DONE
                            }
                        }
                    });
                }
            }
            model.supervisor_id = trap.supervisor_id;
            evnt6create(model);
        }
    },

    evnt6create: evnt6create,

    evnt7create: evnt7create,

    evnt7close: async (model) => {
        let trap = await strapi.controllers.trap.find({
            query: {
                qr_value: model.qr_value
            }
        });

        let evnt07 = await strapi.controllers.action.find({
            query: {
                Type: strapi.config.currentEnvironment.EVNT_07_ACTIONTYPE,
                RelatedAsset: model.qr_value,
                Status: strapi.config.currentEnvironment.EVNT_PENDING,
                supervisor_id: trap[0].supervisor_id,
                RelatedAssetType: strapi.config.currentEnvironment.EVNT_RELATEDASSETTYPE_TRAP
            }
        });
        for (let evnt of evnt07) {
            strapi.controllers.action.update({
                params: { _id: evnt._id },
                request: {
                    body: { Status: strapi.config.currentEnvironment.EVNT_DONE }
                }
            });
        }

        model.supervisor_id = trap[0].supervisor_id;
        evnt7create(model);
    }
}

module.exports = { EVNT };