'use strict';

/**
 * Cron config that gives you an opportunity
 * to run scheduled jobs.
 *
 * The cron format consists of:
 * [MINUTE] [HOUR] [DAY OF MONTH] [MONTH OF YEAR] [DAY OF WEEK] [YEAR (optional)]
 */

const axios = require('axios');

const synchronize = async (data, find, create, update) => {
  for (let item of data) {
    let ret = await find({
      query: {
        uuid: item.uuid
      }
    });
    ret = JSON.parse(ret.body);
    if (ret.items.length === 0) {

      create({
        request: {
          body: item
        }
      })
    }
    else {
      update({
        params: {
          id: ret.items[0].id
        },
        request: {
          body: item
        }
      })
    }
  }
};

function exceededThreshold(lastReadingTime, numberOfDays) {
  var curTime = Math.floor(Date.now() / 60000);
  return curTime - lastReadingTime > numberOfDays * 24 * 60;
}

module.exports = {

  /**
   * Simple example.
   * Every monday at 1am.
   * '0 1 * * 1'
   */

  '0 0 * * *': async () => {
    let devices = await strapi.controllers.device.find({
      query: {
        state: 'Collecting data'
      }
    });

    devices = JSON.parse(devices.body).items;

    for (let item of devices) {
      console.log('number of days ' + item.numberOfDays);
      if (exceededThreshold(item.lastReadingTime, item.numberOfDays)) {

        console.log(item._id);
        await strapi.controllers.device.update({
          params: {
            _id: item._id
          },
          request: {
            body: { state: 'Ready for collection' }
          }
        });
      }
    }
    console.log('Execution finished successfully');
  }  
};
