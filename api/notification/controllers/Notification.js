'use strict';

/**
 * Notification.js controller
 *
 * @description: A set of functions called "actions" for managing `Notification`.
 */

module.exports = {

  /**
   * Retrieve notification records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      let ret = await strapi.services.notification.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    } else {
      let ret = await strapi.services.notification.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    }
  },

  /**
   * Retrieve a notification record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.notification.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Count notification records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.notification.count(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Create a/an notification record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.notification.add(ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Update a/an notification record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.notification.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Destroy a/an notification record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.notification.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  nakheel_mobile_list_notifications: async (ctx) => {
    ctx.query.user_id = ctx.request.body.user_id;
    return strapi.controllers.notification.find(ctx);
  }
};
