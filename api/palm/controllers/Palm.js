'use strict';
const uuid = require('uuid');

/**
 * Palm.js controller
 *
 * @description: A set of functions called "actions" for managing `Palm`.
 */

const find = async (ctx) => {
  if (ctx.query._q) {
    let ret = await strapi.services.palm.search(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);
  } else {
    let ret = await strapi.services.palm.fetchAll(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);
  }
}

const update = async (ctx, next) => {
  let ret = await strapi.services.palm.edit(ctx.params, ctx.request.body);
  return strapi.controllers.responseutils.responseWithItems(ret);
};


module.exports = {

  /**
   * Retrieve palm records.
   *
   * @return {Object|Array}
   */

  find,
  /**
   * Retrieve a palm record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.palm.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Count palm records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.palm.count(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Create a/an palm record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.palm.add(ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Update a/an palm record.
   *
   * @return {Object}
   */

  update,

  /**
   * Destroy a/an palm record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.palm.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  newPalm: async (ctx) => {
    ctx.request.body.uuid = 'P-' + uuid.v1();
    ctx.request.body.health_status = 'Analyzing';
    let matchingType = ctx.request.body.matching_type;

    let palmCtx = {
      query: {
        health_status_ne: 'Unknown'
      }
    }
    if (matchingType === 'qr') palmCtx.query.qr_value = ctx.request.body.qr_value;
    else palmCtx.query.deviceuid = ctx.request.body.deviceuid;

    let palmData = await strapi.controllers.palm.find(palmCtx);
    palmData = JSON.parse(palmData.body);
    for (let item of palmData.items) {
      strapi.controllers.palm.update({
        params: {
          _id: item._id
        },
        request: {
          body: {
            health_status: 'Unknown'
          }
        }
      })
    }
    let devData;
    if (matchingType === 'qr') {
      devData = await strapi.controllers.device.find({
        query: {
          qr_value: ctx.request.body.qr_value
        }
      })
      devData = JSON.parse(devData.body).items[0].uuid;
    }
    else devData = ctx.request.body.deviceuid
    ctx.request.body.deviceuid = devData
    return strapi.controllers.palm.create(ctx);
  },

  getPalm: async (ctx) => {
    ctx.query.health_status_ne = 'Unknown';
    if (ctx.request.body.matching_type) {
      let devCtx = {
        query: {

        }
      };
      if (ctx.request.body.matching_type === 'qr') {
        ctx.query.qr_value = ctx.request.body.value;
        devCtx.query.qr_value = ctx.request.body.value;
      }
      else if (ctx.request.body.matching_type === 'devid') {
        ctx.query.deviceuid = ctx.request.body.value;
        devCtx.query.deviceuid = ctx.request.body.value;

      }
      else return ctx.notFound();

      let dev = await strapi.controllers.device.find(devCtx);

      dev = JSON.parse(dev.body);
      let ret = await strapi.controllers.palm.find(ctx);
      ret = JSON.parse(ret.body);
      if (dev.items.length > 0) {
        ret.items[0].lastConfigTime = dev.items[0].lastConfigTime;
        ret.items[0].lastUplinkTime = dev.items[0].lastUplinkTime;
      }
      ret.items = ret.items[0];
      return ret;
    }
    else {
      console.log(ctx.request.body.palm_id);
      ctx.query.uuid = ctx.request.body.palm_id;
      let ret = await strapi.controllers.palm.find(ctx);
      ret = JSON.parse(ret.body);
      // console.log(ret);
      let dev = await strapi.controllers.device.find({
        query: {
          uuid: ret.items[0].deviceuid,
          qr_value: ret.items[0].qr_value
        }
      });
      dev = JSON.parse(dev.body);
      console.log(dev);
      if (dev.items.length > 0) {
        ret.items[0].lastConfigTime = dev.items[0].lastConfigTime;
        ret.items[0].lastUplinkTime = dev.items[0].lastUplinkTime;
      }
      return ret;
    }
  },

  markHealed: async (ctx) => {
    let palmData = await strapi.controllers.palm.find({
      query: {
        uuid: ctx.request.body.uuid
      }
    });

    ctx.params._id = JSON.parse(palmData.body).items[0]._id
    delete ctx.request.body.uuid;
    ctx.request.body.health_status = 'Healed';
    let ret = await update(ctx);
    ret = JSON.parse(ret.body);
    return ret;
  },

  nakheelble_MarkHealed: async (ctx) => {
    let palmData = await strapi.controllers.palm.find({
      query: {
        qr_value: ctx.request.body.qr_value
      }
    });

    palmData.body = JSON.parse(palmData.body);
    if (palmData.body.items.length === 0) {
      let ret = await strapi.controllers.responseutils.responseForError('Could not read palm with the specified qr');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
    else {
      // adding to palm log
      strapi.controllers.palmlog.create({
        request: {
          body: {
            type: strapi.config.currentEnvironment.PALMLOG_PALMTREATMENT,
            qr_value: ctx.request.body.qr_value,
            supervisor_id: palmData.body.items[0].supervisor_id
          }
        }
      })

      ctx.params._id = palmData.body.items[0]._id
      delete ctx.request.body.uuid;
      ctx.request.body.health_status = 'Recently healed';
      let ret = await update(ctx);
      ret = await strapi.controllers.responseutils.responseMessage('Health updated');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
  },

  nakheel_int_palm_infected: async (ctx) => {
    ctx.query.health_status = 'Infected';
    return strapi.controllers.palm.find(ctx);
  },

  nakheel_QRCode_PalmDetails: async (ctx) => {
    let ret = { items: [] };
    let qr = ctx.request.body.qr_value;
    if (qr.includes('http://')) qr = qr.substring(7);

    let data = await strapi.controllers.palm.find({
      query: {
        qr_value: qr,
        health_status_ne: 'Unknown'
      }
    });
    ret.items.push(data.Items[0]);
    data = await strapi.controllers.device.find({
      query: {
        qr_value: qr
      }
    });

    if (data.Items.length == 0) return strapi.controllers.responseutils.responseForError('no devices exist with this qr');
    else {
      ret.items[0].lastConfigTime = data.Items[0].lastConfigTime;
      ret.items[0].lastUplinkTime = data.Items[0].lastUplinkTime;
      return strapi.controllers.responseutils.responseWithItems(ret.items);
    }
  },

  nakheel_int_palms: async (ctx) => {
    let ret = await strapi.controllers.palm.find({
      query: {
        farm_id: ctx.request.body.farm_id
      }
    });

    return JSON.parse(ret.body);
  },

  nakheelble_InspectPalm_QRScan: async (ctx) => {

    let palm = await strapi.controllers.palm.find({
      query: {
        qr_value: ctx.request.body.qr_value
      }
    });

    palm = JSON.parse(palm.body);
    if (palm.items.length === 0) {
      let ret = await strapi.controllers.responseutils.responseForError('No palm exists with this qr');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
    else if (palm.items[0].supervisor_id !== ctx.request.body.supervisor_id) {
      let ret = await strapi.controllers.responseutils.responseForError('You are not the supervisor of this palm');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
    else {
      let devices = await strapi.controllers.device.find({
        query: {
          qr_value: ctx.request.body.qr_value
        }
      })
      devices = JSON.parse(devices.body);
      let ret = await strapi.controllers.responseutils.responseWithItems({ palms: palm.items, devices: devices.items });
      ret.body = JSON.parse(ret.body);
      return ret;
    }
  },

  nakheel_palmtrees_info: async (ctx) => {
    let palms = await strapi.controllers.palm.find({
      query: { health_status_ne: 'Unknown' }
    })

    palms = JSON.parse(palms.body).items;

    for (let palm of palms) {
      let dev = await strapi.controllers.device.find({
        query: {
          deviceuid: palm.deviceuid,
          qr_value: palm.qr_value,
          state_ne: 'Sensor Dismantled'
        }
      });
      dev = JSON.parse(dev.body).items;
      if (dev.length !== 0) palm.state = dev[0].state;
    }
    let ret = await strapi.controllers.responseutils.responseWithItems({ palms: palms });
    ret.body = JSON.parse(ret.body);
    return ret;
  },

  nakheelble_addPalm: async (ctx) => {
    ctx.request.body.health_status = 'Analyzing';
    ctx.request.body.uuid = 'P-' + uuid.v1();
    ctx.request.body.insertion_time = new Date().getTime().toString();
    let ret = await strapi.controllers.palm.create(ctx);
    ret.body = JSON.parse(ret.body);
    return ret;
  }
}