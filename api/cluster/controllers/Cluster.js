'use strict';

/**
 * Cluster.js controller
 *
 * @description: A set of functions called "actions" for managing `Cluster`.
 */

module.exports = {

  /**
   * Retrieve cluster records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      let ret = await strapi.services.cluster.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    } else {
      let ret = await strapi.services.cluster.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    }
  },

  /**
   * Retrieve a cluster record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.cluster.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Count cluster records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.cluster.count(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Create a/an cluster record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.cluster.add(ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Update a/an cluster record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.cluster.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Destroy a/an cluster record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.cluster.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  nakheel_cluster_db_farms: async (ctx) => {
    ctx.query.cluster_id = ctx.request.body.cluster_id;
    let ret = await strapi.controllers.farm.find(ctx);
    ret = JSON.parse(ret.body);
    return ret;
  },

  nakheel_cluster_db_graphs: async (ctx) => {
    let flags = {
      "quantities": [281, 252, 310, 301, 290],
      "rates": [0.281, 0.252, 0.31, 0.301, 0.29]
    };
    let ret = await strapi.controllers.responseutils.responseWithFlags(flags);
    ret = JSON.parse(ret.body);
    ctx.send(ret);
  },

  nakheel_cluster_db_stats: async (ctx) => {
    let flags = {
      "CIR": 0.13,
      "CIQ": 38,
      "CER": 0.15,
      "CER_arrow": "up",
      "IR": 1
    };
    let ret = await strapi.controllers.responseutils.responseWithFlags(flags);
    ret = JSON.parse(ret.body);
    ctx.send(ret);
  },

  nakheel_list_clusters: async (ctx) => {
    ctx.query.region_id = ctx.request.body.region_id;
    let ret = await strapi.controllers.cluster.find(ctx);
    ret = JSON.parse(ret.body);
    return ret;
  },

  nakheelble_FarmSettings_ClusterList: async (ctx) => {
    ctx.query.region_id = ctx.request.body.region_id;
    let ret = await strapi.controllers.cluster.find(ctx);
    ret.body = JSON.parse(ret.body);
    return ret;
  },

  nakheel_load_cluster: async (ctx) => {
    ctx.query.uuid = ctx.request.body.cluster_id;
    let ret = await strapi.controllers.cluster.find(ctx);
    ret = JSON.parse(ret.body);
    return ret;
  }
};
