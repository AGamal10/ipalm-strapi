'use strict';

/**
 * Wateringmode.js controller
 *
 * @description: A set of functions called "actions" for managing `Wateringmode`.
 */

module.exports = {

  /**
   * Retrieve wateringmode records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.wateringmode.search(ctx.query);
    } else {
      return strapi.services.wateringmode.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a wateringmode record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.wateringmode.fetch(ctx.params);
  },

  /**
   * Count wateringmode records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.wateringmode.count(ctx.query);
  },

  /**
   * Create a/an wateringmode record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.wateringmode.add(ctx.request.body);
  },

  /**
   * Update a/an wateringmode record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.wateringmode.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an wateringmode record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.wateringmode.remove(ctx.params);
  },

  nakheelble_FarmSettings_WateringModesList: async (ctx) => {
    let wmodes = await strapi.controllers.wateringmode.find(ctx);
    let ret = await strapi.controllers.responseutils.responseWithItems(wmodes);
    ret.body = JSON.parse(ret.body);
    return ret;
  }
};
