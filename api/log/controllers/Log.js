'use strict';

/**
 * Log.js controller
 *
 * @description: A set of functions called "actions" for managing `Log`.
 */

module.exports = {

  /**
   * Retrieve log records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      let ret = await strapi.services.log.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    } else {
      let ret = await strapi.services.log.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    }
  },

  /**
   * Retrieve a log record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.log.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Count log records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.log.count(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Create a/an log record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.log.add(ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Update a/an log record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.log.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Destroy a/an log record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.log.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  nakheelble_SensorTimeUpdate: async (ctx) => {
    let dev = await strapi.controllers.device.find({
      query: {
        deviceuid: ctx.request.body.deviceuid,
        state_ne: 'Sensor Dismantled'
      }
    });

    dev = JSON.parse(dev.body).items;
    if (dev.length !== 0) {
      dev = dev[0];
      await strapi.controllers.log.create({
        request: {
          body: {
            activity: 'sensor_time_update',
            time: new Date().getTime().toString(),
            devQR: dev.qr_value,
            devMAC: dev.MAC
          }
        }
      });

      let ret = await strapi.controllers.responseutils.responseMessage('Added to log');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
  }
};
