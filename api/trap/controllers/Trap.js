'use strict';

const uuid = require('uuid');

/**
 * Trap.js controller
 *
 * @description: A set of functions called "actions" for managing `Trap`.
 */

module.exports = {

  /**
   * Retrieve trap records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.trap.search(ctx.query);
    } else {
      return strapi.services.trap.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a trap record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.trap.fetch(ctx.params);
  },

  /**
   * Count trap records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.trap.count(ctx.query);
  },

  /**
   * Create a/an trap record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.trap.add(ctx.request.body);
  },

  /**
   * Update a/an trap record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.trap.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an trap record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.trap.remove(ctx.params);
  },

  nakheelble_addTrap: async (ctx) => {
    ctx.request.body.uuid = 'T-' + uuid.v1();
    let trap = await strapi.controllers.trap.create(ctx);
    let ret = await strapi.controllers.responseutils.responseWithItems(trap);
    ret.body = JSON.parse(ret.body);
    return ret;
  },

  nakheelble_trapService: async (ctx) => {
    let trap = await strapi.controllers.trap.find({
      query: {
        qr_value: ctx.request.body.qr_value
      }
    });

    if (trap.length === 0) {
      return {
        message: 'trap service: trap length = 0'
      }      
    }
    else {
      return strapi.controllers.traplog.create({
        request: {
          body: {
            type: strapi.config.currentEnvironment.TRAPLOG_TYPE_TRAPSERVICE,
            trap: trap[0]._id,
            qr_value: ctx.request.body.qr_value
          }
        }
      })
    }
  }
};
