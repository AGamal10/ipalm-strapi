'use strict';

var uuidv4 = require('uuid/v4');
const EVNT = require('../../../Notification_Engine').EVNT;

const getDate = async () => {
  var months = ["January", "February", "March", "April", "May", "June", "July",
    "August", "September", "October", "November", "December"];
  var d = new Date();
  return `${months[d.getMonth()]} ${d.getDate()}, ${d.getFullYear()}`;
}

const genNewName = async () => {
  return 'D-' + uuidv4();
}

const addToPalmLog = async (palm, health, supervisor_id) => {

  strapi.controllers.palmlog.create({
    request: {
      body: {
        type: strapi.config.currentEnvironment.PALMLOG_HEALTH_EVALUATION,
        health_status: health,
        qr_value: palm.qr_value,
        supervisor_id: supervisor_id
      }
    }
  });
}

/**
 * Device.js controller
 *
 * @description: A set of functions called "actions" for managing `Device`.
 */

module.exports = {

  /**
   * Retrieve device records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      let ret = await strapi.services.device.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    } else {
      let ret = await strapi.services.device.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    }
  },

  /**
   * Retrieve a device record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.device.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Count device records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.device.count(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Create a/an device record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.device.add(ctx.request.body);
    ret = await strapi.controllers.responseutils.responseMessage('Add Item succeeded:{}');
    return JSON.parse(ret.body);
  },

  /**
   * Update a/an device record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.device.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Destroy a/an device record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.device.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  ttn_device_graph: async (ctx) => {
    var rawValues = [];
    var timing = [];
    var end;

    let trouble = ['ec2-0004a30b0023a309', 'ec2-0004a30b0023a188',
      'ec2-0004a30b0023b101', 'ec2-0004a30b0023bfe6', 'ec2-0004a30b00214ef7'];

    function sliceArray(arr, length) {
      var len = arr.length;
      var subarray = [];
      if (len > length) {
        subarray = arr.slice(len - length + 1, len - 1)
        console.log('Sub Array: ' + JSON.stringify(subarray));
        return subarray;
      }
      else {
        console.log('Full Array: ' + JSON.stringify(arr));
        return arr;
      }
    }

    function decode(payload_raw) {
      let buff = new Buffer(payload_raw, 'base64');
      // console.log(buff.toString());
      let text = buff.toString('hex');
      var overallValue = parseInt(text, 16);
      // console.log('"' + payload_raw + '" converted from Base64 to ASCII is "' + text + '"');
      // console.log('Overal Value:' + overallValue);
      return overallValue;
    }

    const accTime = '2018-11-23T13:21:00.000000000Z';

    function badDevice(devid, timing) {
      return trouble.indexOf(devid) > -1 && timing > accTime;
    }

    function withinGap(curRecord, timing) {
      return Math.abs(Date.parse(timing) - curRecord.timing) < strapi.config.currentEnvironment.MINGAP;
    }

    function inBadDay(timing) {
      let date = new Date(timing);
      return date.getMonth() == 10 && (date.getDate() == 29 || date.getDate() == 30);
    }

    function loadRawValues(data, last, devid) {
      // var subdata = sliceArray(data, limit);
      // console.log("DATA HERE IS " + data);
      var subdata = data;
      rawValues = [];
      timing = [];
      // console.log(JSON.stringify(data));
      var curRecord = {
        number: -1,
        timing: 0
      };
      console.log(data);
      for (let val of subdata) {
        // console.log('VAL ' + val['payload_raw']);
        var yourNumber = decode(val['payload_raw']);

        // this is for handlinng devices that accumulate values
        let prevNum = yourNumber;
        if (withinGap(curRecord, val['time'])) continue;
        if (devid == 'ec2-0004a30b0023fca5' && inBadDay(val.time)) continue;
        if (badDevice(devid, val['time']) && curRecord.number != -1)
          yourNumber = yourNumber - curRecord.number;
        if (yourNumber < 0) yourNumber = 0;
        if (yourNumber != 170 && yourNumber != 187 && yourNumber <= 1000) {
          rawValues.push(yourNumber);
          timing.push(val['time']);
          curRecord.number = prevNum;
          curRecord.timing = Date.parse(val.time);
        }
      }
      rawValues = rawValues.slice(Math.max(0, rawValues.length - last));
      timing = timing.slice(Math.max(0, timing.length - last));
      var flags = { 'v': rawValues, 't': timing }
      var resp = strapi.controllers.responseutils.responseWithFlags(flags);
      return resp;
    }

    var devid = ctx.request.body.devid;
    var last = ctx.request.body.last;

    ctx.query.dev_id = devid;
    let data = await strapi.controllers.uplink.find(ctx);
    console.log(data);
    return loadRawValues(JSON.parse(data.body).items, last, devid);
  },

  nakheel_device_palm_info: async (ctx) => {
    let devCtx = {
      query: {
        state_ne: 'Sensor Dismantled'
      }
    };
    if (ctx.request.body.deviceuid) devCtx.query.deviceuid = ctx.request.body.deviceuid;
    else devCtx.query.supervisor_id = ctx.request.body.supervisor_id;

    let palmCtx = {
      query: {
        health_status_ne: 'Unknown'
      }
    };
    if (ctx.request.body.deviceuid) palmCtx.query.deviceuid = ctx.request.body.deviceuid;
    else palmCtx.query.supervisor_id = ctx.request.body.supervisor_id;

    let dev = await strapi.controllers.device.find(devCtx);
    dev = JSON.parse(dev.body);
    if (dev.items.length === 0 && ctx.request.body.deviceuid) {
      let ret = await strapi.controllers.responseutils.responseForError('no device exists with this deviceuid');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
    else {
      let palm = await strapi.controllers.palm.find(palmCtx);

      palm = JSON.parse(palm.body);
      let ret = await strapi.controllers.responseutils.responseWithItems({ palms: palm.items, devices: dev.items });
      ret.body = JSON.parse(ret.body);
      return ret;
    }
  },

  evaluateHealth: async (data) => {
    var config = {
      itemLimit: 5,
      minSamples: 10,
      infectionLimit: 0.7,
      healthLimit: 0.3
    }
    var status = ['Analyzing', 'Infected', 'Healthy', 'Suspicious']
    if (data.length < config.minSamples) {
      return status[0];
    }
    var totalActivities = 0
    data.forEach(element => {
      if (element > config.itemLimit) {
        totalActivities = totalActivities + 1
      }
    });
    totalActivities = totalActivities * 1.0 / data.length
    console.log('total percentage: ' + totalActivities)
    if (totalActivities >= config.infectionLimit) {
      return status[1]
    }
    if (totalActivities <= config.healthLimit) {
      return status[2]
    }
    else {
      return status[3]
    }
  },

  nakheelble_readDeviceData: async (ctx) => {
    let dev = await strapi.controllers.device.find({
      query: {
        deviceuid: ctx.request.body.deviceuid,
        supervisor_id: ctx.request.body.supervisor_id,
        state_ne: 'Sensor Dismantled'
      }
    });
    dev = JSON.parse(dev.body);

    if (dev.items.length === 0) {
      let ret = await strapi.controllers.responseutils.responseForError('No device available with that deviceuid');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
    else {
      const health = await strapi.controllers.device.evaluateHealth(JSON.parse(ctx.request.body.device_data));
      let palm = await strapi.controllers.palm.find({
        query: {
          deviceuid: ctx.request.body.deviceuid,
          health_status_ne: 'Unknown'
        }
      });
      palm = JSON.parse(palm.body);
      if (palm.items.length === 0) {
        let ret = await strapi.controllers.responseutils.responseForError('No palm associated with that device');
        ret.body = JSON.parse(ret.body);
        return ret;
      }
      else {
        console.log(palm.items[0].id);
        let healthReportUpdate;
        if (health === strapi.config.currentEnvironment.HEALTHSTATUS_HEALTHY) EVNT.evnt4create(palm.items[0]), healthReportUpdate = palm.items[0].consecutive_clean_reports + 1;
        else healthReportUpdate = 0;

        await strapi.controllers.palm.update({
          params: {
            _id: palm.items[0]._id
          },
          request: {
            body: {
              health_status: health,
              consecutive_clean_reports: healthReportUpdate
            }
          }
        })

        addToPalmLog(palm.items[0], health, ctx.request.body.supervisor_id);

        // console.log(re);
        let LRD = await getDate();
        console.log(LRD);
        await strapi.controllers.device.update({
          params: {
            _id: dev.items[0]._id
          },
          request: {
            body: {
              device_data: ctx.request.body.device_data,
              lastReadingDate: LRD,
              lastReadingTime: Math.floor(Date.now() / 60000),
              state: 'Collecting data'
            }
          }
        })

        let ret = await strapi.controllers.responseutils.responseMessage(`device ${ctx.request.body.deviceuid} ${health}`);
        ret.body = JSON.parse(ret.body);
        return ret;
      }
    }
  },

  nakheelble_InspectPalm_BLEScan_ReadAllDevices: async (ctx) => {
    let res = {
      Responses: []
    }
    let devices = JSON.parse(ctx.request.body.devices);
    for (let device of devices) {
      let ret = await strapi.controllers.device.nakheelble_readDeviceData({
        request: {
          body: {
            deviceuid: device.deviceuid,
            device_data: device.device_data,
            supervisor_id: device.supervisor_id
          }
        }
      });
      res.Responses.push(ret);
    }

    let ret = await strapi.controllers.responseutils.responseWithItems(res);
    ret.body = JSON.parse(ret.body);
    return ret;
  },

  nakheelble_blescan: async (ctx) => {
    let ret = await strapi.controllers.device.find({
      query: {
        deviceuid: ctx.request.body.deviceuid,
        supervisor_id: ctx.request.body.supervisor_id
      }
    });
    ret.body = JSON.parse(ret.body);
    return ret;

  },

  nakheelble_InstallDevice_QRDeviceName: async (ctx) => {
    let dev = await strapi.controllers.device.find({
      query: {
        qr_value: ctx.request.body.qr_value,
        state_ne: 'Sensor Dismantled'
      }
    });

    dev = JSON.parse(dev.body);
    if (dev.items.length === 0) {
      let newName = await genNewName();
      let ret = await strapi.controllers.responseutils.responseWithFlagsAndItems({ newly_created: true }, {
        deviceuid: newName,
        numberOfDays: 5,
        numberOfHours: 12,
        peakHour: 3
      })
      ret.body = JSON.parse(ret.body);
      return ret;
    }
    else if (ctx.request.body.supervisor_id !== dev.items[0].supervisor_id) {
      let ret = await strapi.controllers.responseutils.responseForError('You are not the supervisor for that device');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
    else {
      let ret = await strapi.controllers.responseutils.responseWithFlagsAndItems({ newly_created: false }, {
        deviceuid: dev.items[0].deviceuid,
        numberOfDays: dev.items[0].numberOfDays || 5,
        numberOfHours: dev.items[0].numberOfHours || 12,
        peakHour: dev.items[0].peakHour || 3
      })
      ret.body = JSON.parse(ret.body);
      return ret;
    }


  },

  nakheelble_InstallDevice: async (ctx) => {
    let dev = await strapi.controllers.device.find({
      query: {
        deviceuid: ctx.request.body.deviceuid,
        state_ne: 'Sensor Dismantled'
      }
    });

    dev = JSON.parse(dev.body);
    for (let item of dev.items) {
      await strapi.controllers.device.update({
        params: {
          _id: item._id
        },
        request: {
          body: {
            state: 'Sensor Dismantled'
          }
        }
      });
    }

    let palm = await strapi.controllers.palm.find({
      query: {
        deviceuid: ctx.request.body.deviceuid,
        supervisor_id: ctx.request.body.supervisor_id,
        health_status_ne: 'Unknown'
      }
    });

    palm = JSON.parse(palm.body);

    for (let item of palm.items) {
      strapi.controllers.palm.update({
        params: { _id: item._id },
        request: {
          body: { health_status: 'Unknown' }
        }
      });
    }

    let reqPalm = await strapi.controllers.palm.find({
      query: {
        qr_value: ctx.request.body.qr_value
      }
    });
    reqPalm = JSON.parse(reqPalm.body);
    if (reqPalm.items.length === 0) {
      let ret = await strapi.controllers.responseutils.responseForError('No palm with this qr value');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
    else {
      reqPalm = reqPalm.items[0];

      let devCtx = ctx;
      devCtx.request.body.state = 'Collecting data';
      devCtx.request.body.device_data = '[]';
      devCtx.request.body.lastReadingTime = Math.floor(Date.now() / 60000).toString();
      devCtx.request.body.lastReadingDate = 'Never';
      devCtx.request.body.lastInstallDate = await getDate();
      devCtx.request.body.numberOfDays = '5';
      devCtx.request.body.numberOfHours = '3';
      devCtx.request.body.peakHour = '12';
      devCtx.request.body.insertion_time = Math.floor(Date.now() / 60000).toString();
      devCtx.request.body.qr_value = reqPalm.qr_value;
      devCtx.request.body.latitude = reqPalm.lat;
      devCtx.request.body.longitude = reqPalm.long;
      console.log(reqPalm);
      await strapi.controllers.device.create(devCtx);
      await strapi.controllers.palm.update({
        params: {
          _id: reqPalm._id
        },
        request: {
          body: {
            deviceuid: ctx.request.body.deviceuid,
            health_status: 'Analyzing'
          }
        }
      });

      let ret = await strapi.controllers.responseutils.responseMessage('Device Installed');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
  }
};
