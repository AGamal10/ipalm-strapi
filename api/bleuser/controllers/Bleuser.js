'use strict';

const bcrypt = require('bcrypt');

/**
 * Bleuser.js controller
 *
 * @description: A set of functions called "actions" for managing `Bleuser`.
 */

module.exports = {

  /**
   * Retrieve bleuser records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.bleuser.search(ctx.query);
    } else {
      return strapi.services.bleuser.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a bleuser record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.bleuser.fetch(ctx.params);
  },

  /**
   * Count bleuser records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.bleuser.count(ctx.query);
  },

  /**
   * Create a/an bleuser record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.bleuser.add(ctx.request.body);
  },

  /**
   * Update a/an bleuser record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.bleuser.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an bleuser record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.bleuser.remove(ctx.params);
  },

  nakheelble_login: async (ctx) => {
    let user = await strapi.controllers.bleuser.find({
      query: {
        login_username: ctx.request.body.login_username
      }
    });

    if (user.length === 0) {
      let ret = await strapi.controllers.responseutils.responseForError('Login Failure');
      ret.body = JSON.parse(ret.body);
      return ret;
    } else {
      const match = await bcrypt.compare(ctx.request.body.login_password, user[0].login_password);

      if (match) {
        let ret = await strapi.controllers.responseutils.responseWithFlags({
          supervisor_id: user[0].supervisor_id
        }, 'Login Successful');
        ret.body = JSON.parse(ret.body);
        return ret;
      } else {
        let ret = await strapi.controllers.responseutils.responseForError('Login Failure');
        ret.body = JSON.parse(ret.body);
        return ret;
      }
    }
  }
};
