'use strict';

/**
 * Action.js controller
 *
 * @description: A set of functions called "actions" for managing `Action`.
 */

module.exports = {

  /**
   * Retrieve action records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    // console.log(ctx.query);
    if (ctx.query._q) {
      return strapi.services.action.search(ctx.query);
    } else {
      return strapi.services.action.fetchAll(ctx.query);
    }
  },

  listActionsForUser: async (ctx) => {
    let today = new Date();
    let myToday = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 2, 0, 0).toISOString();
    let myTomorrow = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1, 2, 0, 0).toISOString();
    let actions = await strapi.controllers.action.find({
      query: {
        supervisor_id: ctx.request.body.supervisor_id,
        DueDate_lt: myTomorrow,
        Status: strapi.config.currentEnvironment.EVNT_PENDING
      }
    })

    let ret = await strapi.controllers.responseutils.responseWithItems(actions);
    ret.body = JSON.parse(ret.body);
    return ret;
  },

  /**
   * Retrieve a action record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.action.fetch(ctx.params);
  },

  /**
   * Count action records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.action.count(ctx.query);
  },

  /**
   * Create a/an action record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.action.add(ctx.request.body);
  },

  /**
   * Update a/an action record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.action.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an action record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.action.remove(ctx.params);
  }
};
