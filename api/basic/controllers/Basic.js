'use strict';
/**
 * Basic.js controller
 *
 * @description: A set of functions called "actions" for managing `Basic`.
 */

module.exports = {

  /**
   * Retrieve basic records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      let ret = await strapi.services.basic.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    } else {
      let ret = await strapi.services.basic.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    }
  },

  /**
   * Retrieve a basic record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.basic.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Count basic records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.basic.count(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Create a/an basic record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.basic.add(ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Update a/an basic record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.basic.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Destroy a/an basic record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.basic.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  nakheel_default_farm: async (ctx) => {
    let ret = await strapi.controllers.responseutils.responseWithFlags({ "region": "1", "cluster_id": "C101", "farm_id": "F101", "country": "UAE" });
    ret = JSON.parse(ret.body);
    ctx.send(ret);
  },

  nakheel_about: async (ctx) => {
    ctx.query.nscope = 'aboutus';
    return strapi.controllers.basic.find(ctx);
  },

  nakheel_db_graphs: async (ctx) => {
    let flags = {
      "quantities": [1829, 1982, 1706, 1783, 1668],
      "rates": [0.3658, 0.3964, 0.3412, 0.3566, 0.3336]
    };
    let ret = await strapi.controllers.responseutils.responseWithFlags(flags);
    ret = JSON.parse(ret.body);
    ctx.send(ret);
  }
};
