'use strict';

/**
 * Palmlog.js controller
 *
 * @description: A set of functions called "actions" for managing `Palmlog`.
 */

module.exports = {

  /**
   * Retrieve palmlog records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.palmlog.search(ctx.query);
    } else {
      return strapi.services.palmlog.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a palmlog record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    console.log('hello', ctx.params);
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.palmlog.fetch(ctx.params);
  },

  /**
   * Count palmlog records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.palmlog.count(ctx.query);
  },

  /**
   * Create a/an palmlog record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.palmlog.add(ctx.request.body);
  },

  /**
   * Update a/an palmlog record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.palmlog.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an palmlog record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.palmlog.remove(ctx.params);
  }
};
