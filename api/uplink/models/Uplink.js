'use strict';

/**
 * Lifecycle callbacks for the `Uplink` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, result) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, results) => {},

  // Fired before a `fetch` operation.
  // beforeFetch: async (model) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, result) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model) => {},

  // After creating a value.
  // Fired after an `insert` query.
  afterCreate: async (model, result) => {
    let activity, details = "";
    if (model.payload_raw === strapi.config.currentEnvironment.CONFIGPAYLOAD) activity = 'config_done';
    else if (model.payload_raw === strapi.config.currentEnvironment.TIMEPAYLOAD) activity = 'time_req';
    else if (model.payload_raw === strapi.config.currentEnvironment.SFPAYLOAD) activity = 'sf_req';
    else activity = 'uplink', details = model.payload_raw;

    let params = {
      query: {
        uuid: model.dev_id.startsWith('ec2') ? model.dev_id.substring(4) : model.dev_id
      }
    }
    let dev = await strapi.controllers.device.find(params)

    let ctx = {
      request: {
        body: {
          activity: activity,
          time: new Date().getTime(),
          devEUI: model.dev_id.startsWith('ec2') ? model.dev_id.substring(4) : model.dev_id,
          devQR: JSON.parse(dev.body).items[0].qr_value,
          details: details
        }
      }
    };
    strapi.controllers.log.create(ctx);
  },

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, result) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, result) => {}
};
