'use strict';

/**
 * Traplog.js controller
 *
 * @description: A set of functions called "actions" for managing `Traplog`.
 */

module.exports = {

  /**
   * Retrieve traplog records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.traplog.search(ctx.query);
    } else {
      return strapi.services.traplog.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a traplog record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.traplog.fetch(ctx.params);
  },

  /**
   * Count traplog records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.traplog.count(ctx.query);
  },

  /**
   * Create a/an traplog record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.traplog.add(ctx.request.body);
  },

  /**
   * Update a/an traplog record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.traplog.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an traplog record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.traplog.remove(ctx.params);
  },

  nakheelble_weevilCount: async (ctx) => {

    let trap = await strapi.controllers.trap.find({
      query: { qr_value: ctx.request.body.qr_value }
    });

    if (trap.length === 0) {
      let ret = await strapi.controllers.responseutils.responseForError('No trap exists with this qr');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
    else {
      ctx.request.body.type = 'weevil_cnt';
      ctx.request.body.trap = trap[0]._id;
      let log = await strapi.controllers.traplog.create(ctx);
      let ret = await strapi.controllers.responseutils.responseWithItems(log);
      ret.body = JSON.parse(ret.body);
      return ret;
    }
  },

  nakheelble_trapImage: async (ctx) => {

    let trap = await strapi.controllers.trap.find({
      query: { qr_value: ctx.request.body.qr_value }
    });

    if (trap.length === 0) {
      let ret = await strapi.controllers.responseutils.responseForError('No trap exists with this qr');
      ret.body = JSON.parse(ret.body);
      return ret;
    }
    else {
      ctx.request.body.type = 'image';
      ctx.request.body.trap = trap[0]._id;
      let log = await strapi.controllers.traplog.create(ctx);
      let ret = await strapi.controllers.responseutils.responseWithItems(log);
      ret.body = JSON.parse(ret.body);
      return ret;
    }
  }
};
