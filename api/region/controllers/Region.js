'use strict';

/**
 * Region.js controller
 *
 * @description: A set of functions called "actions" for managing `Region`.
 */

module.exports = {

  /**
   * Retrieve region records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      let ret = await strapi.services.region.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);

    } else {
      let ret = await strapi.services.region.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);

    }
  },

  /**
   * Retrieve a region record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.region.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);

  },

  /**
   * Count region records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.region.count(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);

  },

  /**
   * Create a/an region record.
   *
   * @return {Object}
   */

  create: async (ctx) => {

    let ret = await strapi.services.region.add(ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);

  },

  /**
   * Update a/an region record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.region.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);

  },

  /**
   * Destroy a/an region record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.region.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);

  },

  nakheel_load_region: async (ctx) => {
    ctx.query.region_id = ctx.request.body.region_id;
    let ret = await strapi.controllers.region.find(ctx);
    ret = JSON.parse(ret.body);
    return ret;
  },

  nakheel_region_db_graphs: async (ctx) => {
    let flags = {
      "quantities": [281, 252, 310, 301, 290],
      "rates": [0.281, 0.252, 0.31, 0.301, 0.29]
    }
    let ret = await strapi.controllers.responseutils.responseWithFlags(flags);
    ret = JSON.parse(ret.body);
    ctx.send(ret);

  },

  nakheel_region_db_stats: async (ctx) => {
    let flags = {
      "CIR": 0.13,
      "CIQ": 38,
      "CER": 0.15,
      "CER_arrow": "up",
      "IR": 1
    }
    console.log(ctx.request.body);
    let ret = await strapi.controllers.responseutils.responseWithFlags(flags);
    ret = JSON.parse(ret.body);
    ctx.send(ret);
  },

  nakhee_int_regions: async (ctx) => {
    let ret = await strapi.controllers.region.find(ctx);
    ret = JSON.parse(ret.body);
    return ret;
  },

  nakheelble_FarmSettings_RegionList: async (ctx) => {
    console.log('hello');
    let ret = await strapi.controllers.region.find(ctx);
    ret.body = JSON.parse(ret.body);
    return ret;
  }
};
