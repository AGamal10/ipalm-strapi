'use strict';

/**
 * Palmtype.js controller
 *
 * @description: A set of functions called "actions" for managing `Palmtype`.
 */

module.exports = {

  /**
   * Retrieve palmtype records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      let ret = await strapi.services.palmtype.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    } else {
      let ret = await strapi.services.palmtype.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    }
  },

  /**
   * Retrieve a palmtype record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.palmtype.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Count palmtype records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.palmtype.count(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Create a/an palmtype record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.palmtype.add(ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Update a/an palmtype record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.palmtype.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Destroy a/an palmtype record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.palmtype.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  nakheel_palm_types: async (ctx) => {
    let ret = await strapi.controllers.palmtype.find(ctx);
    ret = JSON.parse(ret.body);
    for (let item of ret.items) {
      item.name = item.type;
    }
    return ret;
  },

  nakheelble_FarmSettings_PalmTypeList: async (ctx) => {
    let types = await strapi.controllers.palmtype.find(ctx);
    types = JSON.parse(types.body).items;

    let ret = await strapi.controllers.responseutils.responseWithItems({ palmTypes: types });
    ret.body = JSON.parse(ret.body);
    return ret;
  }
};
