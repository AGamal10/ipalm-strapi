'use strict';

/**
 * Fertilizationmode.js controller
 *
 * @description: A set of functions called "actions" for managing `Fertilizationmode`.
 */

module.exports = {

  /**
   * Retrieve fertilizationmode records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.fertilizationmode.search(ctx.query);
    } else {
      return strapi.services.fertilizationmode.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a fertilizationmode record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.fertilizationmode.fetch(ctx.params);
  },

  /**
   * Count fertilizationmode records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.fertilizationmode.count(ctx.query);
  },

  /**
   * Create a/an fertilizationmode record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.fertilizationmode.add(ctx.request.body);
  },

  /**
   * Update a/an fertilizationmode record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.fertilizationmode.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an fertilizationmode record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.fertilizationmode.remove(ctx.params);
  },

  nakheelble_FarmSettings_FertilizationModesList: async (ctx) => {
    let fmodes = await strapi.controllers.fertilizationmode.find(ctx);
    let ret = await strapi.controllers.responseutils.responseWithItems(fmodes);
    ret.body = JSON.parse(ret.body);
    return ret;
  }
};
