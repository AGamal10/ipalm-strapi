'use strict';

/**
 * Activitykind.js controller
 *
 * @description: A set of functions called "actions" for managing `Activitykind`.
 */

module.exports = {

  /**
   * Retrieve activitykind records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.activitykind.search(ctx.query);
    } else {
      return strapi.services.activitykind.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a activitykind record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.activitykind.fetch(ctx.params);
  },

  /**
   * Count activitykind records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.activitykind.count(ctx.query);
  },

  /**
   * Create a/an activitykind record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.activitykind.add(ctx.request.body);
  },

  /**
   * Update a/an activitykind record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.activitykind.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an activitykind record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.activitykind.remove(ctx.params);
  },

  nakheelble_getActivityKinds: async (ctx) => {
    let ak = await strapi.controllers.activitykind.find({
      query: {}
    });
    
    let ret = await strapi.controllers.responseutils.responseWithItems(ak);
    ret.body = JSON.parse(ret.body);
    return ret;
  }
};
